import React, { Component } from 'react';
import '../javacommoncss/javacommon.css'
import { NavLink } from 'react-router-dom'
export default class JavaLeftComponent extends Component {
    render() {
        return (
            <div className="javaLeft">
                <div>
                    <p className="JavaLeftHeader">  JAVA Basic Learn </p>
                    <ul style={{ listStyle: 'none', margin: '0px', padding:'0px' }}>
                        <li className="javaLeftLi" style={{ backgroundColor: this.props.linkName === 'JavaHome' ? '#01a5b9' : 'skyblue' }}><NavLink to='/' className="javaLeftNavLink" >Java-Home</NavLink></li>
                        <li className="javaLeftLi"><NavLink to='/javainstallation' className="javaLeftNavLink">Java-EnvironmentSetup</NavLink></li>
                        <li className="javaLeftLi"><NavLink to='/Javajar' className="javaLeftNavLink" >Jar, War, Ear, Web Server</NavLink></li>
                        <li className="javaLeftLi"><NavLink to='/JavaJDK' className="javaLeftNavLink" > JVM, JRE, JDK</NavLink></li>
                        <li className="javaLeftLi"><NavLink to='/javarules' className="javaLeftNavLink" >Java Coding Rules</NavLink></li>
                        <li className="javaLeftLi"><NavLink to='/javaidentifiers' className="javaLeftNavLink" >Java-Identifiers</NavLink></li>
                        <li className="javaLeftLi"><NavLink to='/javadatatypes' className="javaLeftNavLink" >Java-DataTypes</NavLink></li>
                        <li className="javaLeftLi"><NavLink to='/javaarrays' className="javaLeftNavLink" >Java-Arrays</NavLink></li>
                        <li className="javaLeftLi"><NavLink to='/javapackages' className="javaLeftNavLink" >Java-Packages</NavLink></li>
                        <li className="javaLeftLi"><NavLink to='/javamodifiers' className="javaLeftNavLink">Java-Modifiers</NavLink></li>

                        <li className="javaLeftLi"><NavLink to='/' className="javaLeftNavLink" >Java-Operators</NavLink></li>
                    </ul>


                    <p className="JavaLeftHeader">  JAVA Oops Features </p>
                    <ul style={{ listStyle: 'none', margin: '0px', padding: '0px' }}>
                        <li className="javaLeftLi">Java-Home</li>
                        <li className="javaLeftLi">Java-Installation</li>
                        <li className="javaLeftLi">Java-EnvironmentSetup</li>
                        <li className="javaLeftLi">Java-Syntax</li>
                        <li className="javaLeftLi">Java-Comments</li>
                        <li className="javaLeftLi">Java-DataTypes</li>
                        <li className="javaLeftLi">Java-Variables</li>
                        <li className="javaLeftLi">Java-Constructor</li>
                        <li className="javaLeftLi">Java-Modifiers</li>
                        <li className="javaLeftLi">Java-String</li>
                        <li className="javaLeftLi">Java-Operators</li>
                    </ul>

                    <p className="JavaLeftHeader">  JAVA Exception Handling's </p>
                    <ul style={{ listStyle: 'none', margin: '0px', padding: '0px' }}>
                        <li className="javaLeftLi">Java-Home</li>
                        <li className="javaLeftLi">Java-Installation</li>
                        <li className="javaLeftLi">Java-EnvironmentSetup</li>
                        <li className="javaLeftLi">Java-Syntax</li>
                        <li className="javaLeftLi">Java-Comments</li>
                        <li className="javaLeftLi">Java-DataTypes</li>
                        <li className="javaLeftLi">Java-Variables</li>
                        <li className="javaLeftLi">Java-Constructor</li>
                        <li className="javaLeftLi">Java-Modifiers</li>
                        <li className="javaLeftLi">Java-String</li>
                        <li className="javaLeftLi">Java-Operators</li>
                    </ul>

                    <p className="JavaLeftHeader">  JAVA MultiThreading </p>
                    <ul style={{ listStyle: 'none', margin: '0px', padding: '0px' }}>
                        <li className="javaLeftLi">Java-Home</li>
                        <li className="javaLeftLi">Java-Installation</li>
                        <li className="javaLeftLi">Java-EnvironmentSetup</li>
                        <li className="javaLeftLi">Java-Syntax</li>
                        <li className="javaLeftLi">Java-Comments</li>
                        <li className="javaLeftLi">Java-DataTypes</li>
                        <li className="javaLeftLi">Java-Variables</li>
                        <li className="javaLeftLi">Java-Constructor</li>
                        <li className="javaLeftLi">Java-Modifiers</li>
                        <li className="javaLeftLi">Java-String</li>
                        <li className="javaLeftLi">Java-Operators</li>
                    </ul>
                    <p className="JavaLeftHeader">  JAVA String </p>
                    <ul style={{ listStyle: 'none', margin: '0px', padding: '0px' }}>
                        <li className="javaLeftLi">Java-Home</li>
                        <li className="javaLeftLi">Java-Installation</li>
                        <li className="javaLeftLi">Java-EnvironmentSetup</li>
                        <li className="javaLeftLi">Java-Syntax</li>
                        <li className="javaLeftLi">Java-Comments</li>
                        <li className="javaLeftLi">Java-DataTypes</li>
                        <li className="javaLeftLi">Java-Variables</li>
                        <li className="javaLeftLi">Java-Constructor</li>
                        <li className="javaLeftLi">Java-Modifiers</li>
                        <li className="javaLeftLi">Java-String</li>
                        <li className="javaLeftLi">Java-Operators</li>
                    </ul>
                    <p className="JavaLeftHeader">  JAVA Collection's </p>
                    <ul style={{ listStyle: 'none', margin: '0px', padding: '0px' }}>
                        <li className="javaLeftLi">Java-Home</li>
                        <li className="javaLeftLi">Java-Installation</li>
                        <li className="javaLeftLi">Java-EnvironmentSetup</li>
                        <li className="javaLeftLi">Java-Syntax</li>
                        <li className="javaLeftLi">Java-Comments</li>
                        <li className="javaLeftLi">Java-DataTypes</li>
                        <li className="javaLeftLi">Java-Variables</li>
                        <li className="javaLeftLi">Java-Constructor</li>
                        <li className="javaLeftLi">Java-Modifiers</li>
                        <li className="javaLeftLi">Java-String</li>
                        <li className="javaLeftLi">Java-Operators</li>
                    </ul>
                </div>
            </div>
        );
    }
};