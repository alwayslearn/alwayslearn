import React from 'react';
import JavaHome from './javacomponent/JavaHome';
import JavaInstallation from './javacomponent/JavaInstallation';
import JavaImportant from './javacomponent/JavaImportant';
import JavaJDK from './javacomponent/JavaJDK';
import JavaCodingRules from './javacomponent/JavaCodingRules';
import JavaIdentifiers from './javacomponent/JavaIdentifiers';
import JavaDataTypes from './javacomponent/JavaDataTypes';
import JavaArrays from './javacomponent/JavaArrays';
import JavaModifiers from './javacomponent/JavaModifiers';
import JavaPackages from './javacomponent/JavaPackages';
import { Route } from 'react-router-dom'
function App() {
  return (
    <div className="App">
      <Route exact path={'/'} component={JavaHome}></Route>
      <Route path={'/javainstallation'} component={JavaInstallation}></Route>
      <Route path={'/Javajar'} component={JavaImportant}></Route>
      <Route path={'/JavaJDK'} component={JavaJDK}></Route>
      <Route path={'/javarules'} component={JavaCodingRules}></Route>
      <Route path={'/javaidentifiers'} component={JavaIdentifiers}></Route>
      <Route path={'/javadatatypes'} component={JavaDataTypes}></Route>
      <Route path={'/javaarrays'} component={JavaArrays}></Route>
      <Route path={'/javamodifiers'} component={JavaModifiers}></Route>
      <Route path={'/javapackages'} component={JavaPackages}></Route>
    </div>
  );
}

export default App;
