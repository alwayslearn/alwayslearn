import React, { Component } from 'react';
import '../javacommoncss/javahome.css'
import JavaLeftComponent from '../javacommoncomponent/JavaLeftComponent';
import JavaRightComponent from '../javacommoncomponent/JavaRightComponent'
import Header from '../alwayslearnheader/Header';
import Footer from '../alwayslearnfooter/Footer'
export default class JavaModifiers extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="javahomecombine">
                    <JavaLeftComponent />
                    <div className="javaHomeCenter">
                        <h1 className="javaHomeSpan">What is Java Modifiers ?</h1>
                        <div className="javaText">
                            In java all are Modifiers it does not have access specifiers .
                            Java Modifiers to provide users to access that class property or not.
                           
                            <div style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '3px' }}>
                                <span style={{ color: 'red' ,fontWeight:'bold',fontSize: '16Px',}}> 1. public Modifier  : - </span>
                                <ul>
                                    <li> if we declare a class as a public then we can access that class property
                                         within package or out side of package.  </li>
                                    <li> if we declare a method or a variable as a public then we can access that method
                                    or  variable    within package or out side of package but make sure that class have a public modifier.  </li>
                               
                                    <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}> Example :</h1>
                                <pre style={{ padding: '0px', fontSize: '17px' }}>
                                <span style={{fontWeight:'bold'}}> package p1;<br /> </span>
                                        public class AddTwoNumber{'{'} <br />
                                    <span>     public void add(int a,int b) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           System.out.print(a+b); <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    {'}'}
                                    </pre>
                                </span>
                                </ul>
                            </div>

                            <div style={{ fontSize: '14Px',  paddingBottom: '3px' }}>
                                <span style={{ color: 'red' ,fontWeight:'bold',fontSize: '16Px',}}> 2. protected Modifier  : - </span>
                                <ul>
                                    <li> if we declare a class as a protected then we can access that class property
                                         within package but if you want to access that class property 
                                          out side of package we make sure child class extends declare protected modifier class .  </li>
                                    <li> if we declare a method or a variable as a protected then we can access that method
                                    or  variable    within package or out side of package but make sure that class 
                                    have a public or protected modifier.  </li>
                               
                                    <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}> Example :</h1>
                                <pre style={{ padding: '0px', fontSize: '17px' }}>
                                <span style={{fontWeight:'bold'}}> package p1;<br /> </span>
                                protected class AddTwoNumber{'{'} <br />
                                    <span>     protected void add(int a,int b) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           System.out.print(a+b); <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    {'}'}
                                    </pre>
                                </span>
                                <pre style={{ paddingLeft: '14%', fontSize: '17px' }}>
                                <span style={{fontWeight:'bold'}}> package p2;<br /> </span>
                                <span style={{fontWeight:'bold'}}>import p1.AddTwoNumber;<br /> </span>
                                public class Test extends AddTwoNumber {'{'} <br />
                                    <span>     public void multi(int a , int b) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>         System.out.print(a*b);  <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    {'}'}
                                    </pre>
                                </ul>
                            </div>
                            <div style={{ fontSize: '14Px',  paddingBottom: '3px' }}>
                                <span style={{ color: 'red' ,fontWeight:'bold',fontSize: '16Px',}}> 3. default Modifier  : - </span>
                                <ul>
                                    <li> if we declare a class as a default then we can access that class property
                                         within package but not outside of that package.
                                         </li>
                                    <li> if we declare a method or a variable as a default then we can access that method
                                    or  variable within package.if you want to access that variable or method
                                     outside of package then we sure that class have public or protected modifiers.   </li>
                               
                                    <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                  <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}> Example :</h1>
                                    <pre style={{ padding: '0px', fontSize: '17px' }}>
                                      <span style={{fontWeight:'bold'}}> package p1;<br /> </span>
                                       class AddTwoNumber{'{'} <br />
                                    <span>      void add(int a,int b) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           System.out.print(a+b); <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    {'}'}
                                    </pre>
                                </span>
                                </ul>
                            </div>


                            <div style={{ fontSize: '14Px',  paddingBottom: '3px' }}>
                                <span style={{ color: 'red' ,fontWeight:'bold',fontSize: '16Px',}}> 4. private Modifier  : - </span>
                                <ul>
                                    <li> if we declare a class as a private then we can't access that class property
                                         within package or outside of package.
                                         </li>
                                    <li> if we declare a method or a variable as a private then we can access that method
                                    or  variable within class. we can't access outside of class.  </li>
                               
                                    <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                  <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}> Example :</h1>
                                    <pre style={{ padding: '0px', fontSize: '17px' }}>
                                      <span style={{fontWeight:'bold'}}> package p1;<br /> </span>
                                      private  class AddTwoNumber{'{'} <br />
                                    <span>   private   void add(int a,int b) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           System.out.print(a+b); <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    {'}'}
                                    </pre>
                                </span>
                                </ul>
                            </div>
                            <div style={{ fontSize: '14Px',  paddingBottom: '3px' }}>
                                <span style={{ color: 'red' ,fontWeight:'bold',fontSize: '16Px',}}> 5. final Modifier  : - </span>
                                <ul>
                                    <li> if we declare a class as a final then we can't inherit or extends that class otherwise we will
                                        get compile time error.
                                         </li>
                                    <li> if we declare a method as a final then we can't overide  that method in child class otherwise we will
                                        get compile time error.
                                    </li>
                                    <li> if we declare a variable as a final then we can't reassign that variable value otherwise we will get
                                        compile time error.
                                    </li>
                                    <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                  <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}> Example :</h1>
                                    <pre style={{ padding: '0px', fontSize: '17px' }}>
                                      <span style={{fontWeight:'bold'}}> package p1;<br /> </span>
                                      final  class AddTwoNumber{'{'} <br />
                                    <span>   final   void add(int a,int b) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           System.out.print(a+b); <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    {'}'}
                                    </pre>
                                </span>
                                </ul>
                            </div>


                            <div style={{ fontSize: '14Px',  paddingBottom: '3px' }}>
                                <span style={{ color: 'red' ,fontWeight:'bold',fontSize: '16Px',}}> 6. static Modifier  : - </span>
                                <ul> 
                                </ul>
                            </div>

                            
                            <div style={{ fontSize: '14Px',  paddingBottom: '3px' }}>
                                <span style={{ color: 'red' ,fontWeight:'bold',fontSize: '16Px',}}> 7. abstract Modifier  : - </span>
                                <ul> 
                                </ul>
                            </div>


                            
                            <div style={{ fontSize: '14Px',  paddingBottom: '3px' }}>
                                <span style={{ color: 'red' ,fontWeight:'bold',fontSize: '16Px',}}> 8. native Modifier  : - </span>
                                <ul> 
                                </ul>
                            </div>

                            
                            <div style={{ fontSize: '14Px',  paddingBottom: '3px' }}>
                                <span style={{ color: 'red' ,fontWeight:'bold',fontSize: '16Px',}}> 9. strictfp Modifier  : - </span>
                                <ul> 
                                </ul>
                            </div>


                            <div style={{ fontSize: '14Px',  paddingBottom: '3px' }}>
                                <span style={{ color: 'red' ,fontWeight:'bold',fontSize: '16Px',}}> 10. synchronized Modifier  : - </span>
                                <ul> 
                                </ul>
                            </div>


                        </div>



                    </div>
                    <JavaRightComponent />
                </div>
                <Footer />
            </div>
        )
    }
}