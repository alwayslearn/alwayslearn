import React, { Component } from 'react';
import '../javacommoncss/javahome.css'
import JavaLeftComponent from '../javacommoncomponent/JavaLeftComponent';
import JavaRightComponent from '../javacommoncomponent/JavaRightComponent'
import Header from '../alwayslearnheader/Header';
import Footer from '../alwayslearnfooter/Footer'
export default class JavaArrays extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="javahomecombine">
                    <JavaLeftComponent />
                    <div className="javaHomeCenter">
                        <h1 className="javaHomeSpan">Java Array</h1>
                        <div className="javaText">
                            <ul>
                                <li> Array is collection of homogeneous data element. </li>
                                <li>Array implements Cloneable and Serializable interfaces. </li>
                                <li> Every array start with zero (0) index. </li>
                                <li> Array holds primitive data or object type data. </li>
                                <li>  Array length is defined at the time of creation. so we can't change that length dynamically. </li>
                                <li> Array variable is declared with square bracket like this (String [] name;)  </li>
                            </ul>
                        </div>
                        <h1 className="javaHomeSpan" style={{ paddingTop: '0px', paddingBottom: '0px' }}>Advantage of Array : -</h1>
                        <div className="javaText" >
                            <ul>
                                <li>Array can represents huge number of values in a single variable so that  reduability of the code will be improve.
                        </li>
                                <li> We can randomly access patticular index value. </li>
                            </ul>
                        </div>

                        <h1 className="javaHomeSpan" style={{ paddingTop: '0px', paddingBottom: '0px' }}>Disadvantage of Array : -</h1>
                        <div className="javaText">
                            <ul>
                                <li>Array is fixed in size so we can't change size based on our
                          requirement. </li>
                            </ul>
                        </div>
                        <h1 className="javaHomeSpan" style={{ paddingTop: '0px', paddingBottom: '0px' }}> Array Declaration : -</h1>
                        <div className="javaText">
                            <ul>
                                <li>We can't provide size at the of declaration otherwise we will get compile time error.</li>
                                <pre style={{ fontSize: '14Px', paddingTop: '0px', paddingBottom: '0px' }}>
                                    1 - Dimensional Array Declaration  Example :  int[] x ;
                                   int x[];  int []x ; etc.</pre>
                                <pre style={{ fontSize: '14Px', paddingTop: '0px', paddingBottom: '0px' }}>
                                    2 - Dimensional Array Declaration  Example :  int[][]  x ;
                                   int[]  x[];  int [][]x ; etc. </pre>

                                <pre style={{ fontSize: '14Px', paddingTop: '0px', paddingBottom: '0px' }}>
                                    3 - Dimensional Array Declaration  Example :  int[][]  []x ;
                                   int[]  x[][];  int [][][]x ; etc. </pre>
                                <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                    <h1 style={{ padding: '16px', paddingLeft: '2px', fontSize: '14px', color: 'red' }}>Note </h1>
                                    <div style={{ paddingTop: '0px', fontSize: '15px' }}>
                                        We can specify the dimension before the variable this facility is applicable for 1st variable.
                                        If we are trying to applying next variable we will get compile time error.
                                        <h1 style={{ fontSize: '14Px', paddingTop: '3px', paddingBottom: '0px' }}>
                                            Example :  int[] []x , [] y , []z (y and z variable will get compile time error.)  </h1>
                                    </div>
                                </span>
                            </ul>
                        </div>
                        <h1 className="javaHomeSpan" style={{ paddingTop: '0px', paddingBottom: '0px' }}> Array Creation : -</h1>
                        <div className="javaText">
                            <ul>
                                <li>Array creation is done by the new operator.</li>
                                <li>Once create array every element by default initialized with default values .</li>
                                <li>At the time of array creation we must specify the size otherwise we will get compile time error.</li>
                                <li>If we specify the array size negative value at the time of  array creation then we will get runtime
                                     NegativeArraySizeException. </li>
                                <li>In array creation , we can specify the size of zero this  is legal in java.</li>
                                <li>The allowed data type in array creation is byte , short , char , int . If we are trying
                                any other type we will get compile time error .
                                </li>
                                <h1 style={{ fontSize: '14Px', paddingTop: '3px', paddingBottom: '0px' }}>
                                    For Example :  int[] x=new int[3];  </h1>
                            </ul>
                        </div>
                        <h1 className="javaHomeSpan" style={{ paddingTop: '0px', paddingBottom: '0px' }}> Array Declaration , Creation and Initialization : -</h1>
                        <div className="javaText">
                            we can declare , create and initialized array in a single line.This is shortcut representation of array.
                             <p style={{ padding: '0px', margin: '0px', paddingLeft: '40px' }}> int [] a;</p>
                            <p style={{ padding: '0px', margin: '0px', paddingLeft: '40px' }}> a =new int[2];</p>
                            <p style={{ padding: '0px', margin: '0px', paddingLeft: '40px' }}> a[0] =10;</p>
                            <p style={{ padding: '0px', margin: '0px', paddingLeft: '40px' }}> a[1] =20;</p>
                            <p style={{ padding: '0px', margin: '0px', paddingLeft: '40px' }}> we can represent the above step in a single line like this
                              int[] a={'{'} 10,20 {'}'};</p>
                            <pre style={{ fontSize: '14Px', paddingTop: '0px', paddingBottom: '0px' }}>
                                1-Dimensional Array Declaration,Creation and Initialization in
                                a Single Line  :  int[] x={'{'}10,20,30,40{'}'};
                                  .</pre>
                            {/* <pre style={{ fontSize: '14Px', paddingTop: '0px', paddingBottom: '0px' }}>
                                2-Dimensional  Array Declaration, Creation and Initialization in a Single Line  :
                                 int[][]  x={'{'}{'{'}10,20,30,40{'}'},{'{'}60,80,30,30{'}'}{'}'}; . </pre>

                            <pre style={{ fontSize: '14Px', paddingTop: '0px', paddingBottom: '0px' }}>
                                3-Dimensional Array Declaration,Creation and Initialization in a Single Line :
                                  int[][][]  x={'{'}{'{'}10,20,30,40{'}'},{'{'}60,80,30,30{'}'},{'{'}60,80,30,30{'}'}{'}'};.</pre> */}

                        </div>
                        <h1 className="javaHomeSpan" style={{ paddingTop: '12px', paddingBottom: '10px' }}> Anonymous Arrays in Java : -</h1>
                        <div className="javaText">
                            An  Array which doesn't have name is called Anonymous Array.
                            It is use for  one time requirement.When creating Anonymous Array we can't specifies the size
                            otherwise we will get compile time error.
                            We can also create multidimensional Anonymous Arrays.

                            <h1 style={{ fontSize: '14Px', paddingTop: '3px', paddingBottom: '0px' }}>
                                For Example : new int[] {'{'}10, 30, 40{'}'};  </h1>
                        </div>
                        <div className="javaText">
                            <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}>  Sum of Anonymous Array Element : </h1>
                            <pre style={{ paddingLeft: '60px', fontSize: '17px', fontFamily: 'serif' }}>
                                public class AnonymousArraySum{'{'} <br />
                                <span>     public static void main(String[] args) <br /> </span>
                                <span>    {'{'}<br /> </span>
                                <span>          sumOfAnonymousArra(new int[]{'{'}10,20,30{'}'}); <br /> </span>
                                <span>    {'}'}<br /> </span>

                                <span>     public static void sumOfAnonymousArra(in[] anonymousArray) <br /> </span>
                                <span>    {'{'}<br /> </span>
                                <span>         int totalSum=0; <br /> </span>
                                <span>         for(int arrayData : anonymousArray) <br /> </span>
                                <span>         {'{'}<br /> </span>
                                <span>            totalSum=totalSum+arrayData;<br /> </span>
                                <span>         {'}'}<br /> </span>
                                <span>     System.out.println("Total Sum of Anonymous Array is    "{'  '}+{'     '}totalSum ); <br /> </span>
                                <span>    {'}'}<br /> </span>
                                {'}'}
                            </pre>
                        </div>

                        <h1 className="javaHomeSpan" style={{ paddingTop: '12px', paddingBottom: '10px' }}>Array value assignment rules : -</h1>
                        <div className="javaText">
                            <ul>
                                <li>If we declare or create primitive type array as array element.we can provide
                                any type value which can be implicitly promoted to declared type.
                                    <div style={{ fontFamily: 'monospace', fontSize: '14Px', paddingTop: '3px', paddingBottom: '0px' }}>
                                        For Example : int[]{'    '} array=new int[4];

                                         </div>
                                    <div style={{
                                        fontFamily: 'monospace', fontSize: '14Px',
                                        paddingTop: '3px', paddingLeft: '20%'
                                    }}>

                                        array[0]=20;
                                         </div>
                                    <div style={{
                                        fontFamily: 'monospace', fontSize: '14Px',
                                        paddingTop: '3px', paddingLeft: '20%'
                                    }}>

                                        array[1]='b';
                                         </div>
                                    <div style={{
                                        fontFamily: 'monospace', fontSize: '14Px',
                                        paddingTop: '3px', paddingLeft: '20%'
                                    }}>

                                        byte b=20;
                                         </div>
                                    <div style={{
                                        fontFamily: 'monospace', fontSize: '14Px',
                                        paddingTop: '3px', paddingLeft: '20%'
                                    }}>
                                        array[2]=b;
                                         </div>
                                    <div style={{
                                        fontFamily: 'monospace', fontSize: '14Px',
                                        paddingTop: '3px', paddingLeft: '20%'
                                    }}>

                                        short s=50;
                                         </div>
                                    <div style={{
                                        fontFamily: 'monospace', fontSize: '14Px',
                                        paddingTop: '3px', paddingLeft: '20%'
                                    }}>
                                        array[3]=s;
                                         </div>
                                </li>

                                <li>If we declare or create object  type array as array element.
                                we can provide declare type object or it's child class objects.
                                    <div style={{ fontFamily: 'monospace', fontSize: '14Px', paddingTop: '3px', paddingBottom: '0px' }}>
                                        For Example : Object[]{'    '} array=new Object[2];
                                        </div>
                                    <div style={{
                                        fontFamily: 'monospace', fontSize: '14Px',
                                        paddingTop: '3px', paddingLeft: '20%'
                                    }}>

                                        array[0]=new String("Hello");
                                         </div>
                                    <div style={{
                                        fontFamily: 'monospace', fontSize: '14Px',
                                        paddingTop: '3px', paddingLeft: '20%'
                                    }}>

                                        array[1]=new Integer(20);
                                         </div>
                                </li>
                                <li>If we declare or create interfaces type array as array element.
                                we can provide implemented class type objects.
                                    <div style={{ fontFamily: 'monospace', fontSize: '14Px', paddingTop: '3px', paddingBottom: '0px' }}>
                                        For Example : Runnable[]{'    '} array=new Runnable[2];

                                         </div>
                                    <div style={{
                                        fontFamily: 'monospace', fontSize: '14Px',
                                        paddingTop: '3px', paddingLeft: '20%'
                                    }}>

                                        array[0]=new Thread();
                                         </div>
                                </li>
                                <li>Whenever we are assigning one array to another array both
                                dimensions and type must be match but sizes are not required to match otherwise
                                we will get compile time error.
                                    <div style={{ fontFamily: 'monospace', fontSize: '14Px', paddingTop: '3px', paddingBottom: '0px' }}>
                                        For Example : int[]{'    '} array1=new int[]{'{'} 20,30,50,60{'}'};

                                         </div>
                                    <div style={{
                                        fontFamily: 'monospace', fontSize: '14Px',
                                        paddingTop: '3px', paddingLeft: '13%'
                                    }}>

                                    int[]{'    '} array2=new int[]{'{'} 4,8{'}'};
                                         </div>
                                         <div style={{
                                        fontFamily: 'monospace', fontSize: '14Px',
                                        paddingTop: '3px', paddingLeft: '20%'
                                    }}>
                                         array2=array1;
                                  
                                         </div>
                                </li>
                            </ul>
                        </div>




                    </div>
                    <JavaRightComponent />
                </div>
                <Footer />
            </div>
        )
    }
}