import React, { Component } from 'react';
import '../javacommoncss/javahome.css'
import JavaLeftComponent from '../javacommoncomponent/JavaLeftComponent';
import JavaRightComponent from '../javacommoncomponent/JavaRightComponent'
import Header from '../alwayslearnheader/Header';
import Footer from '../alwayslearnfooter/Footer'
export default class JavaDataTypes extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="javahomecombine">
                    <JavaLeftComponent />
                    <div className="javaHomeCenter">
                        <h1 className="javaHomeSpan">Java DataTypes</h1>
                        <p className="javaText">
                            In java every variable and every expression has some type.
                            These type is called data type. This is cleary defined in each variable and expression.
                            Every variable and expression is  checked by compiler at compile time
                            for purpose of type compatibility.
                            Beacause of this compatibility  Java is a  strongly typed programming language.

                        </p>
                        <h1 className="javaHomeSpan" style={{ paddingTop: '6px', paddingBottom: '6px' }}>Primitive Data Types </h1>
                        <div className="javaText">
                            In java, There are two categories of primitive Data defined which is given below : -
                            <ul style={{ listStyle: 'none' }}>
                                <li > <span style={{ fontSize: '25px' }}>1.</span> Numeric Data Type</li>
                                <li> <span style={{ fontSize: '25px' }}>2.</span>Non-Numeric Data Type</li>
                            </ul>
                        </div>
                        <h1 className="javaHomeSpan" style={{ paddingTop: '6px', paddingBottom: '6px' }}>1. Numeric Data Type</h1>
                        <div className="javaText">
                            Numeric data types also defined two type which is given below: -
                            <ul style={{ listStyle: 'none' }}>
                                <li > <span style={{ fontSize: '25px' }}>A.</span> Integral Data Type</li>
                                <li> <span style={{ fontSize: '25px' }}>B.</span>Floating Data Type</li>
                            </ul>
                        </div>

                        <h1 className="javaHomeSpan" style={{ paddingTop: '6px', paddingBottom: '6px' }}>A. Integral Data Type</h1>
                        <div className="javaText">
                            Integral Data Type also defined four type which is given below: -
                            <ul style={{ listStyle: 'none' }}>
                                <li > <span style={{ fontSize: '25px' }}>i.</span> byte</li>
                                <li> <span style={{ fontSize: '25px' }}>ii.</span>  short</li>
                                <li > <span style={{ fontSize: '25px' }}>iii.</span>  int</li>
                                <li> <span style={{ fontSize: '25px' }}>iv.</span>  long</li>
                            </ul>
                        </div>
                        <h1 style={{ paddingLeft: '20px', fontSize: '18px', color: 'red' }}>
                            i. byte :</h1>
                        <div className="javaText">
                            Accept boolean and char remaining all types are signed data types because
                            we can represent both positive and negative numbers.
                           <h1 style={{ fontSize: '16px', paddingTop: '12px' }}>Size of 1 byte  = 8 bits ,
                              MAX_VALUE of  byte  is +127 , MIN_VALUE of  byte  is -128 , Range of byte is -128 to 127</h1>
                            <p ><span style={{ fontSize: '16px', paddingTop: '0px', color: 'red' }}>Note : </span>
                            if we are using out of range value it gives compile time error CE : possible loss of Pricision.
                            Byte is the best choice if we want to handle data in terms of streams either from the file or from
                            the network (File supported form or network supported form).
                            </p>
                        </div>
                        <h1 style={{ paddingLeft: '20px', fontSize: '18px', color: 'red' }}>
                            ii. short :</h1>
                        <div className="javaText">
                            short data types is rarely used data type in java
                           <h1 style={{ fontSize: '16px', paddingTop: '12px' }}>Size of short  = 16 bits ,
                              Range of short is -(2 power 15) to (2 power 15)-1</h1>
                            <p ><span style={{ fontSize: '16px', paddingTop: '0px', color: 'red' }}>Note : </span>
                            if we are using out of range value it gives compile time error CE : possible loss of Pricision.
                            short data type  is the best suitable for 16 bits processor like 8085 but these processor are
                            completely outdated hence corresponding short data types also outdated.
                            </p>
                        </div>

                        <h1 style={{ paddingLeft: '20px', fontSize: '18px', color: 'red' }}>
                            iii. int :</h1>
                        <div className="javaText">
                            int data types is mostly used data type in java
                           <h1 style={{ fontSize: '16px', paddingTop: '12px' }}>Size of int  = 32 bits ,
                              Range of int is -(2 power 31) to (2 power 31)-1</h1>
                            <p ><span style={{ fontSize: '16px', paddingTop: '0px', color: 'red' }}>Note : </span>
                            if we are using out of range value it gives compile time error CE : possible loss of Pricision.
                            </p>
                        </div>
                        <h1 style={{ paddingLeft: '20px', fontSize: '18px', color: 'red' }}>
                            iii. long :</h1>
                        <div className="javaText">
                            some times int may not enough to hold big values then we should go for long data type.
                           <h1 style={{ fontSize: '16px', paddingTop: '12px' }}>Size of long  = 64 bits ,
                              Range of int is -(2 power 63) to (2 power 63)-1</h1>
                        </div>


                        <h1 className="javaHomeSpan" style={{ paddingTop: '6px', paddingBottom: '6px' }}>B. Floating Data Type</h1>
                        <div className="javaText">
                            Floating Data Type also defined four type which is given below: -
                            <ul style={{ listStyle: 'none' }}>
                                <li > <span style={{ fontSize: '25px' }}>i.</span> float</li>
                                <li> <span style={{ fontSize: '25px' }}>ii.</span>  double</li>
                            </ul>
                        </div>
                        <h1 style={{ paddingLeft: '20px', fontSize: '18px', color: 'red' }}>
                            i. float :</h1>
                        <div className="javaText">
                            If we want to 5 to 6 decimal places of a queriey then we should go for float data type.
                            Float follows single Pricision.
                           <h1 style={{ fontSize: '16px', paddingTop: '12px' }}>
                                Size of float is 4 byte(64 bits)  ,
                              Range of float is -3.4e power 38 to 3.4e power 38 </h1>

                        </div>

                        <h1 style={{ paddingLeft: '20px', fontSize: '18px', color: 'red' }}>
                            i. double :</h1>
                        <div className="javaText">
                            If we want to 14 to 15 decimal places of a queriey then we should go for double
                            data type.
                            Double follows double Pricision.
                           <h1 style={{ fontSize: '16px', paddingTop: '12px' }}>
                                Size of double is 8 byte ,
                              Range of double is -1.7e power 38 to 1.7e power 38 </h1>

                        </div>
                        <h1 className="javaHomeSpan" style={{ paddingTop: '6px', paddingBottom: '6px' }}>2. Non-Numeric Data Type</h1>
                        <div className="javaText">
                            Non-Numeric Data types also defined two type which is given below: -
                            <ul style={{ listStyle: 'none' }}>
                                <li > <span style={{ fontSize: '25px' }}>i.</span> char data type</li>
                                <li> <span style={{ fontSize: '25px' }}>ii.</span>  boolean data type</li>
                            </ul>
                        </div>
                        <h1 style={{ paddingLeft: '20px', fontSize: '18px', color: 'red' }}>
                            i. char data type :</h1>
                        <p className="javaText">
                            In java Language,  Char data type is a unicode based data type and the size of char data
                            type is 2 byte (16 bits) but
                            In old  language like C/C++, Char data type is a ASCII code based data type and the size of
                            char data type is 1 byte (8 bits).
                            The range of char data type in java is 0 to 65536.
                        </p>

                        <h1 style={{ paddingLeft: '20px', fontSize: '18px', color: 'red' }}>
                            i. boolean data type :</h1>
                        <p className="javaText">
                            Boolean data type does not have size.
                            Boolean data type have true or false value if we are providing other
                            value then it gives compile time  error.


                        </p>

                    </div>
                    <JavaRightComponent />
                </div>
                <Footer />
            </div>
        )
    }
}