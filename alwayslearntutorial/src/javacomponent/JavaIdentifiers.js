import React, { Component } from 'react';
import '../javacommoncss/javahome.css'
import JavaLeftComponent from '../javacommoncomponent/JavaLeftComponent';
import JavaRightComponent from '../javacommoncomponent/JavaRightComponent'
import Header from '../alwayslearnheader/Header';
import Footer from '../alwayslearnfooter/Footer'
export default class JavaIdentifiers extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="javahomecombine">
                    <JavaLeftComponent />
                    <div className="javaHomeCenter">
                     <h1 className="javaHomeSpan">What is Java Identifiers</h1>
                        <p className="javaText">
                            A name in a java program is called identifiers which is used for identification 
                            purpose. It can we method name, variable name, class name etc.
                           
                        </p>
                        <h1 className="javaHomeSpan">Rules for Java Identifiers</h1>
                        <div className="javaText">
                           <ul>
                               <li>In java identifiers we can use only a to z , A to Z , 0 to 9 , $ , and _ character if we are using any
                                   other character we will get compile time error.
                               </li>
                               <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 Example : house_number , etc. </h1>

                                 <li> java identifiers should not be start with digit.
                               </li>
                               <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 Example : 123house_number , etc. </h1>
                                 <li> java identifiers are case sensitive because java language is a case sensitive.
                               </li>
                               <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 Example : String address="Delhi" ,  String Address="Delhi" ,  String ADDRESS="Delhi" etc. </h1>

                                 <li>There is no length limit for java identifiers but it is recommanded to use short length. 
                               </li>
                               <li>we can not use reserved words as identifiers.
                               </li>
                               <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 Example :   String if="Delhi" etc. </h1>

                                 <li>All predefined java classes and interfaces are treated as identifiers. 
                               </li>
                           </ul>
                        </div>
                        <h1 className="javaHomeSpan">What is Java Reserved Words</h1>
                        <div className="javaText">
                            In java some words are reserved these reserved words  provide some
                            meaning functionality such type ofg words are called reserved words.  
                            <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 In java there , 50 reserved word and 3 reserved Literals are available which is given below : -</h1>   
                                 <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 <span style={{color:'red'}}> Reserved Keywords for Data Types : - </span> byte ,
                                  short , int , long , float , double , char , boolean.
                                </h1>  
                                <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 <span style={{color:'red'}}> Reserved Keywords for Flow Control Statments : - </span>
                                  if ,default , while , do ,  else , switch , case , for , break , continue, return .
                                </h1>    

                                 <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 <span style={{color:'red'}}> Reserved Keywords for Modifiers : - </span>
                                  public , private , native , strictfp,  protected , static , final ,
                                  , transient , volatile ,  abstract , synchronized ,
                                    .
                                </h1>  

                                <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 <span style={{color:'red'}}> Reserved Keywords for Exception Handling : - </span>
                                try , catch , finally , throw , throws , aseert(1.4) .
                                </h1> 

                                <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 <span style={{color:'red'}}>Some other Reserved Keywords  : - </span>
                               class , interface , extends , implements , package , import .
                                </h1>     
                                <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 <span style={{color:'red'}}> Reserved Keywords for Object : - </span>
                                new ,  instanceof , super , this , return .
                                </h1> 
                                <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 <span style={{color:'red'}}> Unused Reserved Keywords : - </span>
                               const , goto.
                                </h1>


                                <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 <span style={{color:'red'}}> Reserved Literals : - </span>
                              true , false , null.
                                </h1> 

                                <h1 style={{ fontSize: '14Px', paddingTop: '12px', paddingBottom: '12px' }}>
                                 <span style={{color:'red', fontSize: '18Px',}}> Note : - </span>
                                   In java all reserved words are contains only lowercase alphabet symbol.
                                </h1> 
                        </div>
                    </div>
                    <JavaRightComponent />
                </div>
                <Footer />
            </div>
        )
    }
}