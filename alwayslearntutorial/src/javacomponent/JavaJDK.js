import React, { Component } from 'react';
import '../javacommoncss/javahome.css'
import JavaLeftComponent from '../javacommoncomponent/JavaLeftComponent';
import JavaRightComponent from '../javacommoncomponent/JavaRightComponent'
import Header from '../alwayslearnheader/Header';
import Footer from '../alwayslearnfooter/Footer'
export default class JavaJDK extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="javahomecombine">
                    <JavaLeftComponent />
                    <div className="javaHomeCenter">
                     <h1 className="javaHomeSpan">What is JVM (Java Virtual Machine) ?</h1>
                        <p className="javaText">
                            JVM is responsible to run java program line by line. Hence it is Interpreter.
                            JVM is a part of JRE.
                           
                        </p>
                    <h1 className="javaHomeSpan">What is JRE (Java Runtime Environment) ?</h1>
                        <div className="javaText">
                            JRE provides environment to run java programs or java applications.
                            JRE is a part of JDK.
                            <h1 style={{color:'red',fontSize:'12px',padding:'12px'}}> 
                           JRE = JRE (Java Runtime Environment) + Library Classes  </h1>
                        </div>
                        <h1 className="javaHomeSpan">What is JDK ?</h1>
                        <div className="javaText">
                            In Java jdk provides environment to develop and run 
                            java programs or java applications.

                          <h1 style={{color:'red',fontSize:'12px',padding:'12px'}}> 
                           JDK = (JRE (Java Runtime Environment) + Library Classes) + Development Tools  </h1>
                            <img src="../images/JDKImage.png" className="jdkimage" style={{  }} alt="alwayslearnlogo" />
                        </div>

                         <h1 className="javaHomeSpan">How many ways to run a Java Program?</h1>
                        <div className="javaText">
                        There are four ways to run a java programs which is given below : -
                        <ul>
                        <li>In command prompt we can run .class file with java command. </li> 
                         <h1 style={{color:'red',fontSize:'14px',padding:'12px'}}> 
                              For Example   :         java  dotClassName
                          </h1>

                            <li>In command prompt we can run jar file with java command. </li> 
                         <h1 style={{color:'red',fontSize:'14px',padding:'12px'}}> 
                              For Example   :         java -jar dotJarName
                          </h1>
                          <li>By double clicking jar file. </li> 
                        <li style={{padding:'12px',paddingLeft:'0px',paddingBottom:'0px'}}>By double clicking a batch file. </li>
                         <h1 style={{color:'red',fontSize:'12px',padding:'10px'}}> 
                             A batch file contains a group of commands whenever we double clicking the batch file all commands will be
                             executed one by one in the sequence.
                          </h1> 
                          
                         
                          
                        </ul>
                        </div>
                         <h1 className="javaHomeSpan">Java coding Rules</h1>
                        <div className="javaText">
                        Whenever we are writing java code it is highly recommanded to follow these rules : 
                        <ul>
                        <li> Whenever we are writing any component It should be reflect the purpose of that component.
                         The Main advantage of this component is reduability and maintainbility code will be improved.
                        </li>
                        </ul>
                         <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}> Example :</h1>
                                <pre style={{ padding: '0px', fontSize: '17px' }}>
                                    public class A{'{'} <br />
                                    <span>     public static void m(int a,int b) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           System.out.print(a+b); <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    {'}'}


                                </pre>
                            </span>
                            <h1 style={{ padding: '4px', fontSize: '12px', color: 'green' }}>
                               This is not coding standard because it's not showing meaningful definition of class and method.
                            </h1>
                            <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}> Example :</h1>
                                <pre style={{ padding: '0px', fontSize: '17px' }}>
                                    public class TwoNumbersSum{'{'} <br />
                                    <span>     public static void add(int number1,int number2) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           System.out.print(number1+number2); <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    {'}'}


                                </pre>
                            </span>
                            <h1 style={{ padding: '4px', fontSize: '12px', color: 'green' }}>
                               This is coding standard because it's  showing meaningful definition of class and method.
                            </h1>
                        </div>
                    </div>
                    <JavaRightComponent />
                </div>
                <Footer />
            </div>
        )
    }
}