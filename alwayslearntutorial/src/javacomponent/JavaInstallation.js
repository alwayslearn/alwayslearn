import React, { Component } from 'react';
import '../javacommoncss/javahome.css'
import JavaLeftComponent from '../javacommoncomponent/JavaLeftComponent';
import JavaRightComponent from '../javacommoncomponent/JavaRightComponent'
import Header from '../alwayslearnheader/Header';
import Footer from '../alwayslearnfooter/Footer'
import { Link } from 'react-router-dom'
export default class JavaInstallation extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="javahomecombine">
                    <JavaLeftComponent />
                    <div className="javaHomeCenter">
                        <h1 className="javaHomeSpan">How to download JDK </h1>
                        <p className="javaText">
                            We need to go this site
                            <Link to='https://www.oracle.com/in/java/technologies/javase-downloads.html'>
                                Download Link</Link>  and download jdk.
                        </p>
                        <p className="javaText">
                            After download JDK you need to install JDK.
                            </p>

                        <h1 className="javaHomeSpan">What is javac Command </h1>
                        <p className="javaText">
                            javac is a java compiler which is preset in JDK Bin Folder.
                            By using javac command to compile a single or group of java source file and convert into  .Class file.
                        </p>
                        <h1 className="javaHomeSpan">What is java Command </h1>
                        <p className="javaText">
                            java command is preset in JDK Bin Folder.
                            By using java command to run a single .Class file only.
                         

                        </p>
                        <span style={{flexDirection: 'row',display:'inline-flex'}}>
                             <h1 style={{ padding: '16px', fontSize: '18px',color:'red' }}> Note :</h1>
                                <p style={{ padding: '0px', fontSize: '17px' }}>We can compile any number of source file at a time but
                                we can run only one Class file at a time. </p>
                            </span>

                            <h1 className="javaHomeSpan">What is classpath </h1>
                        <p className="javaText">
                           ClassPath is describles the locaation required .class file are available . 
                             Java compiler and JVM will use classpath to locate the required .class file.
                        </p>

                        <h1 className="javaHomeSpan">How to many ways to set JDK path </h1>
                        <p className="javaText">
                            There are two ways to set JDK path which is given below : - 
                            </p>
                           <ul>
                               <li>By Using Command prompt</li>
                               <li>By Using Environment Variable</li>
                           </ul>
                         
                      
                        <h1 className="javaHomeSpan">To set JDK path by using command prompt </h1>
                        <p className="javaText">
                                    By using command prompt we can set jdk path only that command.
                                    When we  closes command prompt automatically class path will be lost.               
                            </p>
                        <p className="javaText">
                            The following step is given to set the JDK path in Windows 10 command prompt :-
                            </p>
                                <ul>
                                <li>Open command prompt ( press Windows Key + R and enter cmd after entered cmd press enter button)</li>
                                <li>Copy the path of jdk/bin folder</li>
                                <li>In command prompt enter :  set path=copied of jdk/bin path like this set path=C:\Program Files\Java\jdk-14.0.1\bin</li>
                                 <li> We can see that the jdk version By using java -version command.</li>
                            </ul>
                       
                            <h1 className="javaHomeSpan">To set JDK path by using environment variable </h1>
                             <p className="javaText">
                                  By using  environment variable we can set JDK path permanantly and it will available
                                  when  we are restarting the system.
                             </p>
                             <p className="javaText">
                                The following step is given to set the JDK path permanantly in Windows 10 :-
                                </p>
                                <ul>
                                    <li>Go to MyComputer and Right click at MyComputer -{' > '} 
                                    click the Properties -{' > '} click Advanced system settings 
                                    -{' > '}click Environment Variables  (After clicking Advanced system settings make sure the open tab is Advanced Tab) 
                                    -{' > '} in Environment Variables check path variable already have or not if not have click User variables New button,
                                     After clicking New button enter path in variable name and paste jdk/bin path in variable value , click ok Button
                                     <p className="javaText">
                                          If path varible already preset click System Variables Edit button(After select path variable),
                                           click new Button and paste the jdk/bin path , click Ok button
                                         </p>
                                    </li> 
                                   </ul>
                               

                            <h1 className="javaHomeSpan">How to run a Java Program </h1>
                            {/* <p className="javaText">
                            </p> */}
                            <h1  style={{ padding: '13px', fontSize: '16px',color:'red' }}>
                                     Example :
                                </h1>
                                <span style={{flexDirection: 'row',paddingLeft:'20px',display:'inline-flex'}}>
                                <pre style={{ padding: '0px', fontSize: '17px' }}>
                                    public class HelloDemo{'{'} <br/>
                                                <span>     public static void main(String args[]) <br/> </span>
                                                    <span>    {'{'}<br/> </span>
                                                    <span>          System.out.print("Hello World"); <br/> </span>
                                                    <span>    {'}'}<br/> </span>
                                           {'}'}
                            <h1  style={{ padding: '3px', fontSize: '14px',color:'red' }}>
                                      Save the above program HelloDemo.java
                            </h1>        
                            <h1  style={{ padding: '3px', fontSize: '14px',color:'red' }}>
                                  Compile Java Program : javac HelloDemo.java   
                            </h1>
                            <h1  style={{ padding: '3px', fontSize: '14px',color:'red' }}>
                            Run Java Program : java  HelloDemo  
                           
                            </h1>
                            <h1  style={{ padding: '3px', fontSize: '14px',color:'red' }}>
                            Output : Hello World  
                            </h1>
                                     </pre>
                            </span>
                            
                    </div>
                    <JavaRightComponent />
                </div>
                <Footer />
            </div>
        )
    }
}