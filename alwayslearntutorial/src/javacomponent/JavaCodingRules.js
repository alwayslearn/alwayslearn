import React, { Component } from 'react';
import '../javacommoncss/javahome.css'
import JavaLeftComponent from '../javacommoncomponent/JavaLeftComponent';
import JavaRightComponent from '../javacommoncomponent/JavaRightComponent'
import Header from '../alwayslearnheader/Header';
import Footer from '../alwayslearnfooter/Footer'
export default class JavaCodingRules extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="javahomecombine">
                    <JavaLeftComponent />
                    <div className="javaHomeCenter">
                        <h1 className="javaHomeSpan">Coding rules for classes</h1>
                        <div className="javaText">
                            Usually class name treated as Nouns so class name should start with
                            uppercase of the first letter and if it contains multiple words then every inner words
                            should starts with uppercase of the first letter.

                             <h1 style={{ fontSize: '14Px', paddingTop: '12px' }}>Example : StringBuilder, StringBuffer, Account ,LoanAccount etc.</h1>

                        </div>
                        <h1 className="javaHomeSpan">Coding rules for interfaces</h1>
                        <div className="javaText">
                            Usually interface name treated as adjectives so interface name should start with
                            uppercase of the first letter and if it contains multiple words then every inner words
                            should starts with uppercase of the first letter.

                             <h1 style={{ fontSize: '14Px', paddingTop: '12px' }}>Example : Comparable, Serializable, Runnable  etc.</h1>

                        </div>


                        <h1 className="javaHomeSpan">Coding rules for methods</h1>
                        <div className="javaText">
                            Usually method name treated as verbs or verb-nouns so method name should start with
                            lowercase and if it contains multiple words then every inner words
                            should starts with uppercase of the first letter.

                             <h1 style={{ fontSize: '14Px', paddingTop: '12px' }}>
                                Example : print(), start(), run(), getLoan(), getName()  etc.</h1>

                        </div>

                        <h1 className="javaHomeSpan">Coding rules for variables</h1>
                        <div className="javaText">
                            In java variable name should start with lowercase alphabet symbol and
                            if it is contaions multiple words then every inner words should start with uppercase of
                            the first letter.

                             <h1 style={{ fontSize: '14Px', paddingTop: '12px' }}>
                                Example : address, mobileNumber, martialStatus,  name, fatherName  etc.</h1>

                        </div>

                        <h1 className="javaHomeSpan">Coding rules for constant</h1>
                        <div className="javaText">
                            In java constant name should contains only uppercase character and
                            if it is contaions multiple words then these words are separated with underscore symbol.
                            Usually constant name should be declare  with public static final modifiers.
                             <h1 style={{ fontSize: '14Px', paddingTop: '12px' }}>
                                Example : MIN_PRIORITY, NORM_PRIORITY, MAX_VALUE, PI  etc.</h1>

                        </div>

                        <h1 className="javaHomeSpan">Coding rules for JavaBean</h1>
                        <div className="javaText">
                            A javabeans is a simple java classes which contains private variables and public getter setter methods.
                            <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}> Example :</h1>
                                <pre style={{ padding: '0px', fontSize: '17px' }}>
                                    public class StudentAddress{'{'} <br />
                                    <span>      private String address; <br /> </span>
                                    <span>     public void setAddress(String address) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           this.address=address; <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    <span>     public String getAddress() <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           return address; <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    {'}'}


                                </pre>
                            </span>
                        </div>
                        <h1 className="javaHomeSpan">Coding rules for Setter Methods</h1>
                        <div className="javaText">
                            <ul>
                                <li> The setter methods modifiers should be public . </li>
                                <li> The setter methods return type  should be void . </li>
                                <li> The setter methods name should be prefixed with set . </li>
                                <li>It should be take some argument i.e. It should not be no argument methods. </li>
                            </ul>
                        </div>

                        <h1 className="javaHomeSpan">Coding rules for Getter Methods</h1>
                        <div className="javaText">
                            <ul>
                                <li> The getter methods modifiers should be public . </li>
                                <li> The getter methods return type  should not be void . </li>
                                <li> The getter methods name should be prefixed with get . </li>
                                <li>It should not be take any argument . </li>
                            </ul>

                            <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                <h1 style={{ padding: '16px', fontSize: '16px', color: 'red' }}>Note </h1>
                                <p style={{ padding: '0px', fontSize: '17px' }}>
                                    For boolean properties getter method name can be prefixed with either get or is
                                    but recommanded to use is.
                                 </p>
                            </span>



                            <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}> Example :</h1>
                                <pre style={{ padding: '0px', fontSize: '17px' }}>

                                    <span>    private boolean empty; <br /> </span>
                                    <span>     public boolean getEmpty()) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           return empty; <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    <span>     public boolean isEmpty() <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           return empty; <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                </pre>
                            </span>
                        </div>
                    </div>
                    <JavaRightComponent />
                </div>
                <Footer />
            </div>
        )
    }
}