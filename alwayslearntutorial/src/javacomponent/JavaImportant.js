import React, { Component } from 'react';
import '../javacommoncss/javahome.css'
import JavaLeftComponent from '../javacommoncomponent/JavaLeftComponent';
import JavaRightComponent from '../javacommoncomponent/JavaRightComponent'
import Header from '../alwayslearnheader/Header';
import Footer from '../alwayslearnfooter/Footer'
export default class JavaImportant extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="javahomecombine">
                    <JavaLeftComponent />
                    <div className="javaHomeCenter">
                        <h1 className="javaHomeSpan">What is Jar file ?</h1>
                        <div className="javaText">
                            Jar file is a combination of .Class file.
                            By using this command to create jar file.
                                <h1 style={{ padding: '10px', fontSize: '16px', color: 'red' }}>
                                jar -cvf JarfileName.jar  space  FileNameofdotClass.class
                                </h1>
                            <h1 style={{ padding: '10px', fontSize: '16px', color: 'red' }}>
                                jar -cvf JarfileName.jar  space  *.* (for all .class file's)
                                </h1>
                                By using this command to extract the jar file.

                                <h1 style={{ padding: '10px', fontSize: '16px', color: 'red' }}>
                                jar -xvf JarfileName.jar
                                </h1>
                            <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}> Example :</h1>
                                <pre style={{ padding: '0px', fontSize: '17px' }}>
                                    public class SquareExample{'{'} <br />
                                    <span>     public static void add(int data1,int data2) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>           System.out.print(data1+data2); <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    {'}'}


                                </pre>
                            </span>
                            <h1 style={{ padding: '4px', fontSize: '12px', color: 'green' }}>
                                javac SquareExample.java
                            </h1>
                            <h1 style={{ padding: '4px', fontSize: '12px', color: 'green' }}>
                                jar -cvf  SquareExample.jar SquareExample.class
                            </h1>

                            <h1 style={{ padding: '10px', fontSize: '16px', color: 'red' }}>
                                Using jar file to run java code :
                                </h1>
                            <span style={{ flexDirection: 'row', paddingLeft: '20px', display: 'inline-flex' }}>
                                <pre style={{ padding: '0px', fontSize: '17px' }}>
                                    public class SquareExampleClient{'{'} <br />
                                    <span>     public static void main(String[] args) <br /> </span>
                                    <span>    {'{'}<br /> </span>
                                    <span>          SquareExample.add(10,20); <br /> </span>
                                    <span>    {'}'}<br /> </span>
                                    {'}'}

                                    <h1 style={{ padding: '1px', fontSize: '14px', color: 'green' }}>
                                        Compile java code :   javac -cp SquareExample.jar  SquareExampleClient.java
                            </h1>
                                    <h1 style={{ padding: '2px', fontSize: '14px', color: 'green' }}>
                                        <pre>Run java code : java -cp SquareExample.jar  SquareExampleClient.java  </pre>
                                        <pre>Output : 30</pre>
                                    </h1>
                                </pre>
                            </span>
                            <span style={{ flexDirection: 'row', display: 'inline-flex' }}>
                                <h1 style={{ padding: '16px', fontSize: '18px', color: 'red' }}> Note :</h1>
                                <p style={{ padding: '0px', fontSize: '17px' }}>We need to add
                                jar file in that place where we save SquareExampleClient.java file.
                                 </p>
                            </span>

                        </div>
                        <h1 className="javaHomeSpan">What is War file (Web Archieve) ?</h1>
                        <p className="javaText">
                            A War file represents one web application which contains JSP , HTML , CSS , Javascript , Servlet etc.
                            The main advantage of war file is to maintaining the project deployment and project delivery .

                        </p>
                        <h1 className="javaHomeSpan">What is Ear file (Enterprise Archieve) ?</h1>
                        <div className="javaText">
                            An Ear file represents one enterprise application which contains JSP , EJB , Servlet , JMS component etc.
                            <pre>In General Ear file represents a group of war files and jar files.</pre>
                        </div>
                        <h1 className="javaHomeSpan">What is Web Server ?</h1>
                        <div className="javaText">
                            Web Server provides environment to run web application.
                            Web server provides support for web technologies like Servlet, JSP, HTML, CSS etc.
                          <h1 style={{ fontSize: '14Px', paddingTop: '12px' }}>Example : Tomcat Server etc.</h1>
                        </div>

                        <h1 className="javaHomeSpan">What is Application Server ?</h1>
                        <div className="javaText">
                            An Application Server provides environment to run Enterprise application.
                            Application Server provides support for any technologies like Servlet, JSP, EJB, HTML, CSS etc.
                          <h1 style={{ fontSize: '14Px', paddingTop: '12px' }}>Example : Weblogic , JBoss etc.</h1>
                        </div>

                    </div>
                    <JavaRightComponent />
                </div>
                <Footer />
            </div>
        )
    }
}