import React, { Component } from 'react';
import '../javacommoncss/javahome.css'
import JavaLeftComponent from '../javacommoncomponent/JavaLeftComponent';
import JavaRightComponent from '../javacommoncomponent/JavaRightComponent'
import Header from '../alwayslearnheader/Header';
import Footer from '../alwayslearnfooter/Footer'

export default class JavaHome extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="javahomecombine">
                    <JavaLeftComponent  linkName='JavaHome' />


                    <div className="javaHomeCenter">
                        <h1 className="javaHomeSpan"> What is java </h1>
                        <p className="javaText">Java is a Object Oriented , Simple , Platform Independent , Multithreaded ,
                        High Performance , Secure Programming language developed by Sun Microsystems
                        and released in 1995. </p>

                        <h1 className="javaHomeSpan"> How many types of Java Platform </h1>
                        <p className="javaText">There are 4 types of Java Platform :    </p>

                        <ul>
                            <li>Java Standard Edition (Java SE) </li>
                            <li>Java Enterprise Edition (Java EE) </li>
                            <li>Java Micro Edition (Java ME) </li>
                            <li>Java FX </li>
                        </ul>

                        <h1 className="javaHomeSpan"> Java Standard Edition (Java SE) </h1>
                        <p className="javaText">Java Standard Edition (Java SE) provides the core
                        functionality of the Java programming language like java.lang, java.io, java.net, java.util, java.sql etc.
                        It defines everything from the basic types like OOPs, String, Regex, Exception, Multithreading, I/O Stream,  Reflection, Collection etc.
                           </p>

                           <h1 className="javaHomeSpan"> Java Enterprise Edition (Java EE) </h1>
                           <p className="javaText">Java Enterprise Edition (Java EE) is used to develop web and
                            enterprise applications. 
                            
                            It defines all topics like Web Services, EJB, JPA, Servlet, JSP,  etc.
                               </p>

                               <h1 className="javaHomeSpan"> Java Micro Edition (Java ME) </h1>
                               <p className="javaText"> Java Micro Edition (Java ME) is used to develop mobile applications.</p>

                               <h1 className="javaHomeSpan"> Java FX  </h1>
                               <p className="javaText"> Java FX  is used to develop internet applications.</p>
                               <h1 className="javaHomeSpan">  Features of  Java Programming Language  </h1>
                               <p className="javaText"> The Features of Java Programming Language is given below : </p>
                               <p className="javaText">  <span style={{marginLeft:'60Px'}}>1. Secured </span></p>
                               <p className="javaText">  <span style={{marginLeft:'60Px'}}>2. Platform independent </span></p>
                               <p className="javaText">  <span style={{marginLeft:'60Px'}}>3. Robust </span></p>
                               <p className="javaText">  <span style={{marginLeft:'60Px'}}>4. Object-Oriented </span></p>
                               <p className="javaText">  <span style={{marginLeft:'60Px'}}>5. Simple </span></p>
                               <p className="javaText">  <span style={{marginLeft:'60Px'}}>6. Multithreaded </span></p>
                               <p className="javaText">  <span style={{marginLeft:'60Px'}}>7. High Performance </span></p>
                               <p className="javaText">  <span style={{marginLeft:'60Px'}}>8. Distributed </span></p>
                               <p className="javaText">  <span style={{marginLeft:'60Px'}}>9. Interpreted </span></p>
                               <p className="javaText">  <span style={{marginLeft:'60Px'}}>10. Portable  , etc.</span></p>
                    
                               <h1 className="javaHomeSpan">1. Secured </h1>
                               <p className="javaText"> Java Programming Language  is a
                                secured programming language i.e It is virus free programming
                                 language because Classloader is used to load Java .Classes file into JVM (Java RunTime Machine) dynamically.
                                  </p>

                                  <h1 className="javaHomeSpan">2. Platform independent </h1>
                                  <p className="javaText"> Java  is a
                                    Platform Independent Programing i.e. Java source code  can 
                                     run many Platform like Windows , Mac , Linux , etc.
                                     It is possible only because JVM is Platform dependent i.e. Windows JVM , Mac JVM , Linux JVM are different
                                  </p>
                                  <h1 className="javaHomeSpan">3. Robust</h1>
                                  <p className="javaText">  *  Java  has Strong Memory Management.  </p>
                                  <p className="javaText">  *  Java  has Garbase Collection mechanisms so we don’t need to delete the unreferenced objects manually.  </p>
                                  <p className="javaText">  *  Java  has exception handling and the type checking mechanism.  </p>
                                  <p className="javaText">  *  Java  does not support explicit pointers so It's avoids security problems. </p>
                                  <h1 className="javaHomeSpan">4. Object-Oriented</h1>
                                  <p className="javaText">Every things defined in java are objects. Java follows the Basic concepts of OOPs features is Objects, Classes , Inheritance , 
                                  Encapsulation / Data hiding , Abstraction , Polymorphism.    </p>

                                  <h1 className="javaHomeSpan">5. Simple</h1>
                                  <p className="javaText"> Java is simple and easy syntax Programming Language which can be learn every people.  </p>
                                  <h1 className="javaHomeSpan">6. Multithreaded </h1>
                                  <p className="javaText"> By using Multithreading concept we can  perform many tasks simultaneously. Multithreading shares a common memory area.    </p>
                                  <h1 className="javaHomeSpan">7. High Performance</h1>
                                  <p className="javaText"> Java is faster than other Programing Language. 
                                  Just-In-Time compilers provides high performance.  </p>
                                  <h1 className="javaHomeSpan">8. Distributed</h1>
                                  <p className="javaText"> Java provides facility to create distributed applications by using RMI and EJB. 
                                  It's also
                                  make able us to access files by calling the methods from any machine on the internet.
                                    </p>

                                    <h1 className="javaHomeSpan">9. Interpreted </h1>
                                  <p className="javaText"> Java byte code is translate to native machine
                                   language and is not stored locally or anywhere. 
                                    </p>

                                    <h1 className="javaHomeSpan">10. Portable  </h1>
                                  <p className="javaText"> we can run Java source file or  .Class file any Platform there is 
                                  no dependent implementation .
                                    </p>
                  
                    </div>
                    <JavaRightComponent />
                </div>
                <Footer />
            </div>
        )
    }
}