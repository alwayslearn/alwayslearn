import React, { Component } from 'react';
import '../alwayslearncommoncss/common.css'
import { NavLink } from 'react-router-dom'
export default class Footer extends Component {
    render() {
        return (
            <div className="footerOuter">
                <div>
                    <img src="../images/Logo.png" className="footerlogo" style={{ width: '6rem', height: '2rem' }} alt="alwayslearnlogo" />
                </div>
                <div className="footerInner">
                    <ul className="footerUl">
                        <li className="footerLI"> <NavLink to='' className="footerNavLink">About Us</NavLink></li>
                        <li className="footerLI"> <NavLink to='' className="footerNavLink">Terms of use</NavLink></li>
                        <li className="footerLI"><NavLink to='' className="footerNavLink">FAQ's</NavLink></li>
                        <li className="footerLI"><NavLink to='' className="footerNavLink">Contact us</NavLink></li>
                    </ul>
                </div>
            </div>
        );
    }
};



