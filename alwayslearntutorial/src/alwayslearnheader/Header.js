import React, { Component } from 'react';
import '../alwayslearncommoncss/common.css'
import AppBar from '@material-ui/core/AppBar';
import { NavLink } from 'react-router-dom'
export default class Header extends Component {
    render() {
        return (
            <AppBar position="static">
                <div className="headerOuter">
                    <img src="../images/Logo.png" className="headerlogo" style={{ width: '10rem', height: '4rem' }} alt="alwayslearnlogo" />
                </div>
                <div className="headerInner">
                    <ul className="ulHeader">
                        <li className="headerLI"> <NavLink to='' className="headerNavLink">Home</NavLink></li>
                        <li className="headerLI"><NavLink to='' className="headerNavLink"  >Java</NavLink></li>
                        <li className="headerLI"><NavLink to='' className="headerNavLink">MongoDB</NavLink></li>
                        <li className="headerLI"><NavLink to='' className="headerNavLink">Nodejs</NavLink></li>
                    </ul>

                </div>
            </AppBar>
        );
    }
};



